package com.example.myapplication

//import android.os.Parcel
//import android.os.Parcelable
import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Entity
data class Task (
    @PrimaryKey(autoGenerate = true) val identifier: Int,
    @ColumnInfo(name = "first_name") val someVal: String
) {

}

@Dao
interface TaskDao {
    @Query("SELECT * FROM task")
    fun getAll(): List<Task>

    @Query("SELECT * FROM task ORDER BY identifier ASC")
    fun getSorted(): Flow<List<Task>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(vararg users: Task)

    @Query("DELETE FROM task")
    suspend fun deleteAll()


}